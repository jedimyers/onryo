﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueMindTracker : MonoBehaviour {

    public NPCMind mind;
    public Text relations;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (mind != null)
            relations.text = mind.playerRelations.ToString();

	}
}
