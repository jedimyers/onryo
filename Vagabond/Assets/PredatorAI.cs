﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorAI : CombatAI {
    public int randomMovement;
    public float directionTimers;
    public Stats health;
    public WolfAIStates curState;
    public Animator AIanimator;

    // Update is called once per frame
    void Update() {

        if (health.HP <= 0) {
            AIanimator.SetBool("Circling", false);
            AIanimator.SetBool("Attacking", false);
            AIanimator.SetBool("Dying", true);

            curState = WolfAIStates.Dying;
        }

        if (curState == WolfAIStates.Idle) {
            AIanimator.SetBool("Circling", false);
            AIanimator.SetBool("Attacking", false);
            if (NearestHostile() != null)
                target = NearestHostile().transform;

            if (target != null)
            {
                Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
                float curDist = Vector3.Distance(target.position, pos);
                if (curDist <= combatRange)
                {
                    curState = WolfAIStates.Circling;
                }
                else
                {
                    curState = WolfAIStates.Walking;
                }
            }
        }
        if (curState == WolfAIStates.Walking) {

            if (NearestHostile() != null)
                target = NearestHostile().transform;

            if (target != null)
            {
                Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
                float curDist = Vector3.Distance(target.position, transform.position);
                Debug.Log(curDist);
                if (curDist <= combatRange)
                {
                    curState = WolfAIStates.Circling;
                }
                else
                {
                    transform.LookAt(target.transform.position);
                }

                transform.Translate(Vector3.forward * Time.deltaTime * speed);
            }
        }
        if (curState == WolfAIStates.Circling)
        {
            AIanimator.SetBool("Circling", true);
            transform.LookAt(target.transform.position);
            directionTimers += Time.deltaTime;
            if (directionTimers > 0.5f)
            {
                randomMovement = Random.Range(0, 2);
                directionTimers = 0f;
            }
            if (randomMovement == 0)
            {
                if (Physics.Raycast(transform.position, Vector3.right, 1f))
                {
                    randomMovement = 1;
                    //transform.Translate (Vector3.right * 3f * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector3.right * 4f * Time.deltaTime);
                }
            }
            else if (randomMovement == 1)
            {
                if (Physics.Raycast(transform.position, Vector3.right, -1f))
                {
                    randomMovement = 0;
                    //transform.Translate (Vector3.right * -3f * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector3.right * 4f * Time.deltaTime);
                }
            }
            else if (randomMovement == 2)
            {
                if (Physics.Raycast(transform.position, Vector3.forward, -1f))
                {
                    randomMovement = 3;
                    //transform.Translate (Vector3.forward * 0.5f * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector3.forward * 0.5f * Time.deltaTime);
                }
            }

            AITimer += Time.deltaTime;

            if (AITimer >= 2f)
            {
                AIanimator.SetBool("Circling", false);
                AIanimator.SetBool("Attacking", true);
                curState = WolfAIStates.Pouncing;
                AITimer = 0;
            }

        }
        if (curState == WolfAIStates.Pouncing)
        {

        }
        if (curState == WolfAIStates.Attacking) {
            transform.Translate(Vector3.forward * 15f * Time.deltaTime);
        }

        if (curState == WolfAIStates.Dying) {
            AITimer += Time.deltaTime;
            if (AITimer >= 1) {
                Destroy(this);
            }
        }


    }

    public void ChangeState(WolfAIStates state) {
        curState = state;
    }

    public enum WolfAIStates {
        Idle,
        Walking,
        Circling,
        Pouncing,
        Attacking,
        Dying
    }


}
