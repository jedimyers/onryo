﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class QuestButton : MonoBehaviour {

    public Text name;
    public StoryObjects story;
    public questUI questLogic;
    public bool npcStory;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SelectQuest() {
        questLogic.questDescription.text = story.stageDescription;
        questLogic.questName.text = story.questName;
        if (npcStory) {
            questLogic.tradeStory = story;
        }
        else
        {
            questLogic.selectedStory = story;
        }
    }


}
