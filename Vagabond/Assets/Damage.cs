﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

    public float damageValue;

    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "character") {
            other.GetComponent<Stats>().HP -= damageValue;
        }
    }


}
