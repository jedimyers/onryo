﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transparency : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerEnter(Collider col){
		if (col.tag == "transparency") {
			col.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.3f);
		}
	}

	void OnTriggerExit(Collider col){
		if (col.tag == "transparency") {
			Color tmp = col.gameObject.GetComponent<SpriteRenderer>().color;
			tmp.a = 1f;
			col.gameObject.GetComponent<SpriteRenderer>().color = tmp;
		}
	}
}
