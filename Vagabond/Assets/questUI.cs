﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class questUI : MonoBehaviour {

	public GameObject questButtons;
	public Transform content;
    public GameObject NPCPanel;
    public Transform NPCcontent;
    public List<Button> quests;
    public List<GameObject> questButtonList;
    public List<GameObject> NPCquestButtonList;
    public List<Button> npcQuests;
	public stories story1;
    public stories npcStory1;
    public Text questName;
    public Text questDescription;
    public StoryObjects selectedStory;
    public StoryObjects tradeStory;
    public NPCMind NPCmind;
    public GameObject dialogueOptions;
    public Text DialogueBox;


	// Use this for initialization
	void Start () {
        QuestUpdate();

        if (npcStory1 == null)
        {
            NPCPanel.SetActive(false);
        }
        else      
        {
            NPCPanel.SetActive(true);
            NPCQuestUpdate();
        }
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            //QuestUpdate();
           // NPCQuestUpdate();
        }
    }

    public void storyDisplay(quests quest) {
        questName.text = quest.name;
        if(quest.stage1 && !quest.stage2)
            questDescription.text = quest.stage1Description;
        if (quest.stage1 && quest.stage2 && !quest.stage3)
            questDescription.text = quest.stage1Description + " " + quest.stage2Description;
        if (quest.stage1 && quest.stage2 && quest.stage3)
            questDescription.text = quest.stage1Description + " " + quest.stage2Description + " " + quest.stage3Description;
    }

    public void ExitMenu() {
        NPCquestButtonList.Clear();
        npcStory1 = null;
        this.gameObject.SetActive(false);
    }

    void QuestUpdate() {
        int a;
        for (a = 0; a < questButtonList.Count; a++)
        {
            Destroy(questButtonList[a]);
        }
        questButtonList.Clear();

        int i;
        for (i = 0; i < story1.questList.Count; i++)
        {
            GameObject obj;
            obj = Instantiate(questButtons, content.position, content.rotation);
            obj.GetComponent<QuestButton>().name.text = story1.questList[i].questName;
            obj.GetComponent<QuestButton>().story = story1.questList[i];
            obj.transform.SetParent(content);
            obj.transform.localScale = Vector3.one;
            obj.GetComponent<QuestButton>().questLogic = this;
            questButtonList.Add(obj);
        }
    }

    public void ReturnToDialogue()
    {
        NPCquestButtonList.Clear();
        npcStory1 = null;
        dialogueOptions.SetActive(true);
        this.gameObject.SetActive(false);
    }

    public void DisplayDialogueLine(string line) {
        DialogueBox.text = line;
    }

    void NPCQuestUpdate()
    {
        int a;
        for (a = 0; a < NPCquestButtonList.Count; a++)
        {
            Destroy(NPCquestButtonList[a]);
        }
        NPCquestButtonList.Clear();

        int i;
        for (i = 0; i < npcStory1.questList.Count; i++)
        {
            GameObject obj;
            obj = Instantiate(questButtons, NPCcontent.position, NPCcontent.rotation);
            obj.GetComponent<QuestButton>().name.text = npcStory1.questList[i].questName;
            obj.GetComponent<QuestButton>().story = npcStory1.questList[i];
            obj.GetComponent<QuestButton>().questLogic = this;
            obj.GetComponent<QuestButton>().npcStory = true;
            obj.transform.SetParent(NPCcontent);
            obj.transform.localScale = Vector3.one;
            NPCquestButtonList.Add(obj);
        }
    }

    public void AttemptToAddToList() {
        if (!story1.questList.Contains(tradeStory))
        {
            story1.questList.Add(tradeStory);
            NPCQuestUpdate();
            QuestUpdate();
        }
    }


    public void AttemptToAddToNPCList() {
        if (!npcStory1.questList.Contains(selectedStory))
        {
            npcStory1.questList.Add(selectedStory);
            NPCmind.CheckPersonalityToTradedStory(selectedStory.Appeals, this, selectedStory);
            NPCQuestUpdate();
            QuestUpdate();
        }
    }
}
