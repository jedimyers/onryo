﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inventory : MonoBehaviour {

	public List<item> items;
	public List<Text> slots;
	public List<Text> slotsEquiped;
	public item current;
	public List<int> quantities;
	public item head;
	public item torso;
	public item weapon;
	public item shield;
	public float water;
	public int maxWater;
	int currentNum;
	public health health;
	public Text drinks;
	public GameObject[] ammo;
	public Text itemText;
	public float DR;
	public float shieldDR;
	public bool ai;

	public Animator armorAnimator;
	public Animator weaponAnimator;

	public RuntimeAnimatorController armorSprite;
	public RuntimeAnimatorController weaponSprite;


	// Use this for initialization
	void Start () {
		for (int i = 0; i < items.Count; i++) {
			if (items[i].type == "canteen") {
				maxWater += 1;
			}
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (!ai) {
			armorAnimator.runtimeAnimatorController = armorSprite;
			//weaponAnimator.runtimeAnimatorController = weaponSprite;
			armorSprite = torso.sprite;

			for (int i = 0; i < items.Count; i++) {
				slots [i].text = items [i].name;
			}
			
			DR = torso.defense + head.defense;
			shieldDR = shield.shieldDefense;

			slotsEquiped [0].text = head.name;
			slotsEquiped [1].text = torso.name;
			slotsEquiped [2].text = weapon.name;
			slotsEquiped [3].text = shield.name;
			slotsEquiped [4].text = ammo [0].GetComponent<item> ().name;

			drinks.text = water.ToString ();
		}
	}

	public void selectItem(int i){
		if (i < items.Count) {
			current = items [i];
			currentNum = i;
			itemText.text = current.itemText;
		}
	}

	public void selectEquippedItem(string i){
		if (i == "head") {
			current = head;
			itemText.text = current.itemText;
		}
		if (i == "torso") {
			current = torso;
			itemText.text = current.itemText;
		}
		if (i == "weapon") {
			current = weapon;
			itemText.text = current.itemText;
		}
		if (i == "shield") {
			current = shield;
			itemText.text = current.itemText;
		}
		if (i == "ammo") {
			current = ammo[0].GetComponent<item>();
		}
	}

	public void equip(){
		if (current.type == "armor") {
			if (current.slot == "head") {
				if (weapon.name != "null") {
					items [currentNum] = head;
				}
				head = current;
			} 
			if (current.slot == "torso") {
				if (weapon.name != "null") {
					items [currentNum] = torso;
				}
				torso = current;
			} 
		}
		if (current.type == "bow"||current.type == "melee") {
			if (current.slot == "weapon") {
				//if (weapon.name == "null") {
				if (weapon.name != null) {
					items [currentNum] = weapon;
				}
				weapon = current;
				//}

			} 
		}

		if (current.type == "food") {
			health.food += items [currentNum].foodValue;
		}
		if (current.type == "arrow") {
			ammo [0] = current.gameObject;
		}
		if (current.type == "canteen") {
			health.water += items [currentNum].foodValue;
			if (health.water > 100) {
				health.water = 100;
			}
			water -= 0.5f;
		}
	}

	public void addItem(item thing){

		items.Add (thing);

		maxWater = 0;

		for (int i = 0; i < items.Count; i++) {
			if (items[i].type == "canteen") {
				maxWater += 1;
			}

		}
	}
}
