﻿using System.Collections;
using System.Collections.Generic;
using Dialog;
using UnityEngine;

namespace Demo {

	public class Player : BaseNPCBehaviour {

		public int orbsCollected = 0;

		private void OnTriggerEnter(Collider other) {
			if (other.gameObject.name == "Orb(Clone)") {
				orbsCollected++;
				OpenDialog(this.transform, orbsCollected);
				Destroy(other.gameObject);
			}
		}
	}
}