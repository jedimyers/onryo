﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Demo {
	public class GameManager : MonoBehaviour {

		public static GameObject CameraRig;
		public static GameObject Player;
		public static GameObject GameObject;

		public GameObject player;
		public GameObject cameraRig;

		void Awake() {
			CameraRig = cameraRig;
			Player = player;
			GameObject = gameObject;
		}

		public void LoadScene(string sceneName) {
			SceneManager.LoadScene(sceneName);
		}

		void Update() { }
	}
}