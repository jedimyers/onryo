﻿using System.Collections;
using System.Collections.Generic;
using Dialog;
using UnityEngine.UI;
using UnityEngine;

namespace Dialogue {
	public class wanderer: BaseNPCBehaviour {

		public GameObject questUI;
		public GameObject dialogueUI;
		public Text content;
        public NPCMind mind;

		// Use this for initialization
		void Start () {

		}

		// Update is called once per frame
		void Update () {

		}

        public override void OpenDialog(Transform invokerTransform)
        {
            if (dialogs.Length > 0)
            {
                DialogIsOpen = true;
                dialogueUI.GetComponent<DialogueMindTracker>().mind = mind;
                OpenDialog(invokerTransform, 0);
            }
            else
            {
                Debug.LogError("No dialogs attached to: " + this.name);
            }
        }

        public void questUiOpens() {
			questUI.SetActive (true);
            questUI.GetComponent<questUI>().npcStory1 = mind.NPCstory;
            questUI.GetComponent<questUI>().NPCmind = mind;
            questUI.GetComponent<questUI>().NPCmind.GetTaleQuestion(questUI.GetComponent<questUI>());
            questUI.GetComponent<questUI>().dialogueOptions.SetActive(false);
            //dialogueUI.SetActive (false);
        }

		public void news(){
			content.text = mind.latestNews;
		}

        public void showDestination()
        {
            content.text = mind.destinationDescriptions[mind.curDestination];
        }



    }
}
