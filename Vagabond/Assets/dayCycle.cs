﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dayCycle : MonoBehaviour {

	public float worldTimer;
	public float dayTimer;
	public Color night;
	public Color dawn;
	public Color day;
	public Color current;
	public Light light;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		light.color = current;

		worldTimer += Time.deltaTime;
		dayTimer += Time.deltaTime;
		if (dayTimer >= 600) {
			dayTimer = 0;
		}

		if (dayTimer <= 300) {
			current = day;
		} else if (dayTimer > 300 && dayTimer < 400){
			current = Color.Lerp (current, dawn, Time.deltaTime * 0.05f);
		} else if (dayTimer > 400){
			current = Color.Lerp (current, night, Time.deltaTime * 0.01f);
		}
	}
}
