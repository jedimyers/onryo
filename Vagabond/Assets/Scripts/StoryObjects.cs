﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class StoryObjects : ScriptableObject {

    public string questName;
    public bool complete;
    public bool unique;
    public string stageDescription;
    public PersonalityTags[] Appeals;
    public float rarity = 1;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
