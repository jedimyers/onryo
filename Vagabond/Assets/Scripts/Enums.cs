﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PersonalityTags {
    Romantic,
    Cynical,
    Cunning,
    Aesthetic,
    Affable,
    Witty,
    Scholarly,
    Bawdy,
    Adventerous,
    Violent,
    Melancholy,
    Entrepreneurial,
    Pacifist,
    Indomitable
}

public enum GenderPreferences {
    Male,
    Female,
    Androgynous,
    Demisexual,
    Asexual,
    Aromantic
}