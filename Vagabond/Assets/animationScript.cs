﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationScript : MonoBehaviour {

	public Animator anim;
	public Vector3 prev;
	// Use this for initialization
	void Start () {
		prev = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (prev != transform.position) {
			anim.SetBool ("walking", true);
		} else {
			anim.SetBool ("walking", false);
		}

		prev = transform.position;
	}
}
