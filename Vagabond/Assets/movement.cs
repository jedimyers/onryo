﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
	public float angle;
	public Vector3 g;
	public Transform transformMoving;
	public float speed;

    public bool canMove;
	public bool selected;
	public float movespeed;
	public float baseSpeed;
    public Animator Anim;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody> ().velocity = Vector3.zero;

        if (!Anim.GetCurrentAnimatorStateInfo(0).IsName("vagabondAttack"))
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector3.forward * movespeed * Time.deltaTime);

            }


            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(-Vector3.right * movespeed * Time.deltaTime);
            }


            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(-Vector3.forward * movespeed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.right * movespeed * Time.deltaTime);
            }
        }

		Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
		Vector3 dir = Input.mousePosition - pos;
		angle = Mathf.Atan2 (dir.x, dir.y) * Mathf.Rad2Deg;
		transformMoving.transform.rotation = Quaternion.AngleAxis (angle, Vector3.up);
			//transform.rotation = Quaternion.Lerp(transform.rotation, transformMoving.rotation, Time.deltaTime * speed);

	}

    public void CanMove(bool a) {
        canMove = a;
    }
}
	
