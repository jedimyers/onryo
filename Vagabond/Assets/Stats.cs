﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

	public float HP;
	public float health;
	public float hunger;
	public float thirst;
	public float exhaustion;
	public float morale;
	public float moraleRate;
	public float healthThirstRate;
	public float healthHungerRate;
	public float healthExhaustionRate;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		hunger -= Time.deltaTime * 0.25f;
		exhaustion -= Time.deltaTime * 0.15f;
		thirst -= Time.deltaTime * 0.5f;
		if (hunger < 250) {
			healthHungerRate = 0.05f;
		} else if (hunger < 200) {
			healthHungerRate = 0.1f;
		} else if (hunger < 150) {
			healthHungerRate = 0.15f;
		} else if (hunger < 100) {
			healthHungerRate = 0.2f;
		} else if (hunger < 50) {
			healthHungerRate = 0.3f;
		} else {
			healthHungerRate = 0f;
		}

		if (exhaustion < 250) {
			healthExhaustionRate = 0.05f;
		} else if (exhaustion < 200) {
			healthExhaustionRate = 0.1f;
		} else if (exhaustion < 150) {
			healthExhaustionRate = 0.2f;
		} else if (exhaustion < 100) {
			healthExhaustionRate = 0.4f;
		} else if (exhaustion < 50) {
			healthExhaustionRate = 0.6f;
		} else {
			healthExhaustionRate = 0f;
		}

		if (thirst < 250) {
			healthThirstRate = 0.1f;
		} else if (thirst < 200) {
			healthThirstRate = 0.2f;
		} else if (thirst < 150) {
			healthThirstRate = 0.4f;
		} else if (thirst < 100) {
			healthThirstRate = 0.5f;
		} else if (thirst < 50) {
			healthThirstRate = 0.7f;
		} else {
			healthThirstRate = 0f;
		}

		//health -= (healthThirstRate + healthHungerRate) * Time.deltaTime;

	}
}
