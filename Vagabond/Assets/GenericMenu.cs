﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Dialog.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GenericMenu : MonoBehaviour {

    public GameObject DialogBox;

    public GameObject CloseButton;

    UnityEvent alt1Triggers = new UnityEvent();
    UnityEvent alt2Triggers = new UnityEvent();
    UnityEvent alt3Triggers = new UnityEvent();
    UnityEvent alt4Triggers = new UnityEvent();
    UnityEvent openTriggers = new UnityEvent();
    UnityEvent closeTriggers = new UnityEvent();
    float timeoutTime = 0;
    float maxTimeout = 0;
    bool isShown = false;
    bool closedHasFired = false;
    Coroutine activeCoroutine;

    BindingFlags bindFlags = BindingFlags.NonPublic | BindingFlags.Instance;

    public virtual void Awake()
    {
        CloseButton.GetComponent<Button>().onClick.AddListener(HideDialog);

        DialogBox.SetActive(false);
    }

    public virtual void HideDialog()
    {
        if (!closedHasFired)
        {
            FireClosedTriggers();
            closedHasFired = true;
        }
        StopActiveCoroutine();
        DeactivateDialogGameObject();
        isShown = false;
    }

    void StopActiveCoroutine()
    {
        if (activeCoroutine != null)
        {
            StopCoroutine(activeCoroutine);
        }
        activeCoroutine = null;
    }

    void FireOpenedTriggers()
    {
        for (int i = 0; i < openTriggers.GetPersistentEventCount(); i++)
        {
            string name = openTriggers.GetPersistentTarget(i).name;
            foreach (var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
            {
                if (gameObj.name == name || gameObj.name == name + "(Clone)")
                {
                    ApplyTrigger(gameObj, openTriggers, i);
                }
            }
        }
    }

    void FireClosedTriggers()
    {
        for (int i = 0; i < closeTriggers.GetPersistentEventCount(); i++)
        {
            string name = closeTriggers.GetPersistentTarget(i).name;
            foreach (var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
            {
                if (gameObj.name == name || gameObj.name == name + "(Clone)")
                {
                    ApplyTrigger(gameObj, closeTriggers, i);
                }
            }
        }
    }

    void FireAlt1Triggers()
    {
        for (int i = 0; i < alt1Triggers.GetPersistentEventCount(); i++)
        {
            string name = alt1Triggers.GetPersistentTarget(i).name;
            foreach (var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
            {
                if (gameObj.name == name || gameObj.name == name + "(Clone)")
                {
                    ApplyTrigger(gameObj, alt1Triggers, i);
                }
            }
        }
    }

    void FireAlt2Triggers()
    {
        for (int i = 0; i < alt2Triggers.GetPersistentEventCount(); i++)
        {
            string name = alt2Triggers.GetPersistentTarget(i).name;
            foreach (var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
            {
                if (gameObj.name == name || gameObj.name == name + "(Clone)")
                {
                    ApplyTrigger(gameObj, alt2Triggers, i);
                }
            }
        }
    }

    void FireAlt3Triggers()
    {
        for (int i = 0; i < alt3Triggers.GetPersistentEventCount(); i++)
        {
            string name = alt3Triggers.GetPersistentTarget(i).name;
            foreach (var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
            {
                if (gameObj.name == name || gameObj.name == name + "(Clone)")
                {
                    ApplyTrigger(gameObj, alt3Triggers, i);
                }
            }
        }
    }

    void FireAlt4Triggers()
    {
        for (int i = 0; i < alt4Triggers.GetPersistentEventCount(); i++)
        {
            string name = alt4Triggers.GetPersistentTarget(i).name;
            foreach (var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
            {
                if (gameObj.name == name || gameObj.name == name + "(Clone)")
                {
                    ApplyTrigger(gameObj, alt4Triggers, i);
                }
            }
        }
    }

    protected virtual void DeactivateDialogGameObject()
    {
        DialogBox.SetActive(false);
    }

    protected virtual void ActivateDialogGameObject()
    {
        DialogBox.SetActive(true);
    }

    void ApplyTrigger(GameObject target, UnityEvent triggers, int index)
    {
        FieldInfo persistantCalls = typeof(UnityEventBase).GetField("m_PersistentCalls", bindFlags);
        var persistentCalls = persistantCalls.GetValue(triggers);
        IEnumerable calls = persistentCalls.GetType().GetField("m_Calls", bindFlags).GetValue(persistentCalls) as IEnumerable;
        List<object> callList = calls.Cast<object>().ToList();
        if (callList.Count > 0)
        {
            var persistentCall = callList[index].GetType();
            var argCache = persistentCall.GetField("m_Arguments", bindFlags).GetValue(callList[index]);
            PersistentListenerMode mode = (PersistentListenerMode)persistentCall.GetField("m_Mode", bindFlags).GetValue(callList[index]);
            var argument = new object();
            switch (mode)
            {
                case PersistentListenerMode.Bool:
                    argument = (bool)argCache.GetType().GetField("m_BoolArgument", bindFlags).GetValue(argCache);
                    break;
                case PersistentListenerMode.Float:
                    argument = (float)argCache.GetType().GetField("m_FloatArgument", bindFlags).GetValue(argCache);
                    break;
                case PersistentListenerMode.String:
                    argument = (string)argCache.GetType().GetField("m_StringArgument", bindFlags).GetValue(argCache);
                    if (triggers.GetPersistentMethodName(index) == "SendMessage")
                    {
                        target.SendMessage((string)argument);
                        return;
                    }
                    break;
                case PersistentListenerMode.Int:
                    argument = (int)argCache.GetType().GetField("m_IntArgument", bindFlags).GetValue(argCache);
                    break;
                case PersistentListenerMode.Object:
                    argument = argCache.GetType().GetField("m_ObjectArgument", bindFlags).GetValue(argCache);
                    break;
                case PersistentListenerMode.Void:
                    target.SendMessage(triggers.GetPersistentMethodName(index));
                    return;
            }
            target.SendMessage(triggers.GetPersistentMethodName(index), argument);
        }
        else
        {
            target.SendMessage(triggers.GetPersistentMethodName(index));
        }
    }

}
