﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CombatAI : MonoBehaviour {

    public Transform target;
    public List<GameObject> listGameObjectsInTrigger;
    public int health;
    public int maxHealth;
    public float AITimer;
    public float combatRange;
    public CharacterFaction factions;
    public float speed = 4f;
    public Transform mainCamera;
    public float curAngle;
	public float directionTimers;

    public void Start()
    {
        mainCamera = Camera.main.transform;
    }

    public GameObject NearestHostile() {
        GameObject closest = null;
        float distance = Mathf.Infinity;
        foreach (GameObject hostile in listGameObjectsInTrigger) {
            Vector3 diff = hostile.transform.position - transform.position;
            float curDistance = diff.sqrMagnitude;
            if (factions.enemyFactions.Contains(hostile.GetComponent<CharacterFaction>().faction))
            {
                if (curDistance < distance)
                {
                    closest = hostile;
                    distance = curDistance;
                }
            }
        }
        return closest;
    }


}
