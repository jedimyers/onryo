﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class health : MonoBehaviour {

	public float hp;
	public float maxHP;
	public float sanity;
	public float maxSanity;
	public float stamina;
	public float maxStamina;
	public float water;
	public float food;
	public float[] staminaRegenIncrements;

	//attributes

	//ui
	public Slider healthSlider;
	public Slider staminaSlider;
	public Slider thirstSlider;
	public Slider hungerSlider;
	public GameObject shield;
	public bool ai;
	public float staminaRegenHunger;
	public float staminaRegenThrist;
	public float staminaRegen;

	public bool rested;
	public bool defending;
	public bool paused;
	float timer;
	public float otherTimer;

	// Use this for initialization
	void Start () {
		hp = maxHP;
		stamina = maxStamina;
	}
	
	// Update is called once per frame
	void Update () {
		if (!ai) {
			healthSlider.maxValue = maxHP;
			staminaSlider.maxValue = maxStamina;
			healthSlider.value = hp;
			staminaSlider.value = stamina;
			thirstSlider.value = water;
			hungerSlider.value = food;

			if (water > 75) {
				staminaRegenThrist = staminaRegenIncrements[0];
			}
			if (water < 75 && water > 50) {
				staminaRegenThrist = staminaRegenIncrements[1];
			}
			if (water < 50 && water > 25) {
				staminaRegenThrist = staminaRegenIncrements[2];
			}
			if (water < 25 && water > 2) {
				staminaRegenThrist = staminaRegenIncrements[3];
			}

			if (food > 75) {
				staminaRegenHunger = staminaRegenIncrements[0];
			}
			if (food < 75 && water > 50) {
				staminaRegenHunger = staminaRegenIncrements[1];
			}
			if (food < 50 && water > 25) {
				staminaRegenHunger = staminaRegenIncrements[2];
			}
			if (food < 25 && water > 2) {
				staminaRegenHunger = staminaRegenIncrements[3];
			}
		}
			

		if (defending) {
			paused = true;
		} else {
			paused = false;
		}

		staminaRegen = staminaRegenHunger + staminaRegenThrist;

		if (stamina < 1) {
			defending = false;
			otherTimer += Time.deltaTime;
			paused = true;
			if (otherTimer > 2f) {
				stamina += staminaRegen;
				paused = false;
				otherTimer = 0;
			}
		} else {
			shield.SetActive (defending);
		}
			
		if (!paused) {
			if (stamina < maxStamina) {
				timer += Time.deltaTime;
				if (timer > 1.5f) {
					stamina += staminaRegen;
					timer = 0;
				}
			}
		}
			
		food -= Time.deltaTime / 10f;
		water -= Time.deltaTime / 5f;
	}
}
