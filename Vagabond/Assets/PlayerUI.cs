﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour {

    public Stats stats;
    public float curHealth;
    public GameObject[] healthBar;

	// Use this for initialization
	void Start () {
        curHealth = stats.health;
        foreach (GameObject h in healthBar)
        {
            h.SetActive(false);
        }

        for (int i = 0; i < curHealth; i++)
        {
            healthBar[i].SetActive(true);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (stats.HP < curHealth) {
            foreach (GameObject h in healthBar) {
                h.SetActive(false);
                curHealth = stats.HP;
            }

            for (int i = 0; i < curHealth; i++)
            {
                healthBar[i].SetActive(true);
            }
        }
	}
}
