﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITrigger : MonoBehaviour {

    public CombatAI CharacterAI;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "character" && !CharacterAI.listGameObjectsInTrigger.Contains(other.gameObject)) {
            CharacterAI.listGameObjectsInTrigger.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "character" && CharacterAI.listGameObjectsInTrigger.Contains(other.gameObject))
        {
            CharacterAI.listGameObjectsInTrigger.Remove(other.gameObject);
        }
    }
}
