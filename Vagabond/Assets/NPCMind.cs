﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMind : MonoBehaviour {

    public string name;
    public string latestNews;
    public string[] destinations;
    public string[] destinationDescriptions;
    public int curDestination;
    public stories NPCstory;
    public PersonalityTags[] personalityTags;
    public float playerRelations;
    public float relationshipConstant = 2;
    float relationChange;
    public string[] StoryReactionLines; //0 - Question //1 - Negative //2 - Positive

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CheckPersonalityToTradedStory(PersonalityTags[] a, questUI quest, StoryObjects story) {
        foreach (PersonalityTags trait in personalityTags) {

            foreach (PersonalityTags appeal in a) {
                if (trait == appeal)
                {
                    relationChange += relationshipConstant;
                }
            }
        }

        if (relationChange > 0) {
            quest.DialogueBox.text = StoryReactionLines[2];
        } else {
            quest.DialogueBox.text = StoryReactionLines[1];
        }
        
        playerRelations += (relationChange * story.rarity);
    }


    public void GetTaleQuestion(questUI quest)
    {
        quest.DialogueBox.text = StoryReactionLines[0];
    }

}
