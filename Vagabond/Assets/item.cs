﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class item : MonoBehaviour {
	public string name;
	public string type;
	public string slot;
	public float defense;
	public float shieldDefense;
	public float damage;
	public float speed;
	public float range;
	public float foodValue;
	public float quantity;
	public float staminaCost;
	public inventory inv;
	public string itemText;
	public RuntimeAnimatorController sprite;


	// Use this for initialization
	void Start () {
		inv = GameObject.Find ("Player").GetComponent<inventory> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown (1)) {
			inv.addItem (this);
			this.gameObject.SetActive (false);
		}

	}
}
