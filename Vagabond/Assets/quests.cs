﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class quests : MonoBehaviour {

	public string questName;
	public bool complete;

	public string stage1Description;
	public string stage2Description;
	public string stage3Description;
	public string stage4Description;
	public string stage5Description;
	public bool stage1;
	public bool stage2;
	public bool stage3;
	public bool stage4;
	public bool stage5;
    public string description;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
