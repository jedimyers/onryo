﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAI : MonoBehaviour {

	public FactionAI factionAI;
	public string faction;
	public string orders;

	public Transform[] waypoint; 
	public UnityEngine.AI.NavMeshAgent nav;      // The amount of Waypoint you want
	public float patrolSpeed = 3f;  
	public Vector3 target;
	//public float health;
	public bool attacking;
	public float timer;
	public bool hostile;
	public bool moving;
	//public bool future_enemy;
	private float curTime;
	private int currentWaypoint = 0;
	private CharacterController character;
	public Vector3 moveDirection;
	public bool hostilityTimer;
	public float timerTrigger;
    public bool running;
	public GameObject anim;
	public HealthScript healthScript;
	public float staminaCostLight;

	public int closeRange;
	public bool loop;
	public bool gangfight;
	public bool inRange;
	public List<string> enemyFaction;
	//public string[] enemyFaction;
	public bool possessed;
	public bool boss;
	public gangAI gangs;
	public float longRange;
    public bool engaged;

	public float directionTimers;
	public bool stun;
	public float stunTimer;
	public GameObject stagger;

	public sensor sensor;
	public Stats stats;
	public DamageScript damage;
	public int randomMovement;
    public float timerHostile;
	// Use this for initialization
	void Start () {
		faction = factionAI.faction;
		enemyFaction = gangs.rivals;
		//anim = GetComponentInChildren<Animations>().gameObject;
		//hostile = true;
	}
	
	// Update is called once per frame
	void Update () {
        enemyFaction = gangs.rivals;
        //damage.damage = stats.weapon.damage;

        if (moving) {
            
			if(currentWaypoint < waypoint.Length){
				patrol();
			} else {    
				//if(loop){
				currentWaypoint = 0;
				//} 
			}

		} 

		if (!hostile) {
			for (int i = 0; i < sensor.inTrigger.Count; i++) {
				if (sensor.inTrigger != null) {
					for (int e = 0; e < enemyFaction.Count; e++){
						
						if (sensor.inTrigger [i] != null && sensor.inTrigger [i].GetComponent<factions> ().faction == enemyFaction [e]) {
							hostile = true;
                            gangs.hostile = true;
                            target = sensor.inTrigger[i].transform.position;
                            //nav.SetDestination(target);
                            //gangs.target = sensor.inTrigger[i].gameObject;
                        }
                     
                    }
				} 
			} 
		}
	
		if (hostile) {
            
            for (int i = 0; i < sensor.inTrigger.Count; i++) {
				if (sensor.inTrigger != null) {
					for (int e = 0; e < enemyFaction.Count; e++){

						if (sensor.inTrigger [i] != null && sensor.inTrigger [i].GetComponent<factions> ().faction == enemyFaction [e]) {
                            hostile = true;

                            //inRange = true;
                            gangs.hostile = true;
                            timer = 0;
                            timerHostile = 0;
                            engaged = true;
                        }
                        else {
                            hostile = false;
							gangs.hostile = false;
                            //gangs.reportChange();
                        }
					}
				} 
			} 




			if (FindClosestEnemy () != null) {
                //inRange = false;
				attacking = false;
				//nav.destination = FindClosestEnemy ().transform.position;
				// = FindClosestEnemy ().transform.position;
			} else {
				attacking = false;
				//hostile = false;
				//nav.enabled = true;
			}

			Vector3 fwd = transform.TransformDirection (Vector3.forward);
			RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit, 6f))
            {

                if (hit.transform.tag == "characters")
                {
                    string fact = hit.transform.gameObject.GetComponent<factions>().faction;
                    for (int e = 0; e < enemyFaction.Count; e++)
                    {

                        if (enemyFaction[e] == fact)
                            {
							    if (healthScript.stamina >= 2f)
                                {
                                    RaycastHit hit3;

                                    if (Physics.SphereCast(transform.position, 0.1f, transform.forward, out hit, 6f))
                                    {
									
                                        transform.Translate(Vector3.forward * 0.5f * Time.deltaTime);
										attacking = true;
										nav.enabled = true;
										nav.Stop();
                                    }
                                    else
                                    {
                                        transform.Translate(Vector3.forward * -0.5f * Time.deltaTime);
                                    }
                                }
                                else
                                {
                                    attacking = false;

                                }
                            }
                            else
                            {
                                //nav.Stop ();
                                //transform.Translate (transform.forward * -3.0f * Time.deltaTime);
                            }
                        
                    }
                }

                if (attacking == true)
                {
                    timer += Time.deltaTime;


                    if (timer >= 0.5f)
                    {
                        //if (hit.transform.tag == "characters") {
                        timer = 0;
                        attacking = false;
                        //} 
                    }
                }
                else
                {
                    timer = 0;
                }

            }
            else
            {
                if (nav.enabled) { 
                    nav.Resume();
                }

                if (FindClosestEnemy() != null)
                {
                    if (nav.enabled) {
                    nav.destination = FindClosestEnemy().transform.position;
                    }
                    inRange = false;
                }



            }

		} else {
			//moving = true;
			if (!stun) {
				nav.Resume ();
			}
            timerHostile += Time.deltaTime;
            if (timerHostile >= 6f && engaged == true)
            {
                gangs.reportChange();
                timerHostile = 0;
            }
        }

		/*if (stun && healthScript.health > 0) {
			stunTimer += Time.deltaTime;
			if (stunTimer < 0.2) {
				transform.Translate (Vector3.forward * -5f * Time.deltaTime);
				attacking = false;
				anim.SetActive (false);
				stagger.SetActive (true);
			} else {
				anim.SetActive (true);
				stun = false; 
				stunTimer = 0;
				stagger.SetActive (false);
			}
		
		} else {
		
			stagger.SetActive (false);
		}*/


        if (inRange && stun == false)
        {
            if (!hostile)
            {
                hostile = true;
            }

            directionTimers += Time.deltaTime;
            if (directionTimers > 0.5f)
            {
                randomMovement = Random.Range(0, 3);
                directionTimers = 0f;
            }


            transform.LookAt(FindClosestEnemy().transform.position);
            if (randomMovement == 0)
            {
                if (Physics.Raycast(transform.position, Vector3.right, 1f))
                {
                    randomMovement = 1;
                    //transform.Translate (Vector3.right * 3f * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector3.right * 4f * Time.deltaTime);
                }
            }
            else if (randomMovement == 1)
            {
                if (Physics.Raycast(transform.position, Vector3.right, -1f))
                {
                    randomMovement = 0;
                    //transform.Translate (Vector3.right * -3f * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector3.right * -4f * Time.deltaTime);
                }
            }
            else if (randomMovement == 2)
            {
                if (Physics.Raycast(transform.position, Vector3.forward, -1f))
                {
                    randomMovement = 3;
                    //transform.Translate (Vector3.forward * 0.5f * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector3.forward * 0.5f * Time.deltaTime);
                }
            }
            else if (randomMovement == 3)
            {
                //if (Physics.Raycast (transform.position, transform.forward, 2f)) {
                //	randomMovement = Random.Range (0, 3);
                //} else {
                transform.Translate(-Vector3.forward * 0.5f * Time.deltaTime);
                //}
            }

		}else if (!inRange) {
			nav.enabled = true;
		}

		float curDistance = Vector3.Distance(FindClosestEnemy().transform.position, transform.position);
		if (curDistance > closeRange){
			//hostile = false;
			//gangs.hostile = false;
			//nav.enabled = true;
			inRange = false;

		} else {
			//transform.LookAt(FindClosestEnemy().transform.position);
			inRange = true;
			//nav.enabled = false;
		}

	}

	GameObject FindClosestEnemy() {
		GameObject[] gos;
        //gos = GameObject.FindGameObjectsWithTag("pcs");
        gos = GameObject.FindGameObjectsWithTag("characters");
        //gos = sensor.inTrigger;
        GameObject closest = null;
		float distance = Mathf.Infinity;  //Set initial value
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
            for (int i = 0; i < enemyFaction.Count; i++) {
				if (go.GetComponent<factions> ().faction == enemyFaction [i]) {

					float curDistance = Vector3.Distance(go.transform.position, position);
					if (curDistance < distance) {
						closest = go;
						distance = curDistance;
					} 
					/*if (hostile) {
						
						if (curDistance > longRange) {
							hostile = false;
							gangs.hostile = false;
						}
					}*/
				}
			}
		}
		return closest;
	}

	public void patrol(){
		
		target = waypoint [currentWaypoint].position;
		target.y = transform.position.y; // Keep waypoint at character's height

		moveDirection = target - transform.position;

		if (moveDirection.magnitude < 0.5f) {
			currentWaypoint++;
            if (hostile) {
                nav.speed = 12f;
            } else {
                nav.speed = 5f;
            }
		} else {        
            if(nav.enabled)
			nav.SetDestination (target);
		}

	}
}
