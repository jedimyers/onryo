﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class sensor : MonoBehaviour {


	public List<GameObject> inTrigger;


	// Use this for initialization
	void Start () {
		//sensors = GameObject.FindGameObjectsWithTag("sensors").GetComponent<sensor>();
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < inTrigger.Count; i++)
        {
            if (inTrigger[i].gameObject.GetComponent<HealthScript>().spawnDead)
            {
                inTrigger.Remove(inTrigger[i]);


            }
        }

     }

     void OnTriggerEnter(Collider col){
		if (col.tag == "characters" && !col.GetComponent<HealthScript>().spawnDead) {

			inTrigger.Add (col.transform.gameObject);

			/*for (int i = 0; i < inTrigger.Count; i++) {
				if (inTrigger [i] == null) {
					inTrigger [i] = col.gameObject;

					i = inTrigger.Count;
				}
			}*/
		} else if (col.tag == "characters" && col.GetComponent<HealthScript>().spawnDead) {

			inTrigger.Remove (col.transform.gameObject);

			/*for (int i = 0; i < inTrigger.Count; i++) {
				if (inTrigger [i] == null) {
					inTrigger [i] = col.gameObject;

					i = inTrigger.Count;
				}
			}*/
		}
	}

	void OnTriggerStay(Collider col){
		if (col.tag == "characters" &&col.GetComponent<HealthScript>().spawnDead) {

			inTrigger.Remove (col.transform.gameObject);

			/*for (int i = 0; i < inTrigger.Count; i++) {
				if (inTrigger [i] == null) {
					inTrigger [i] = col.gameObject;

					i = inTrigger.Count;
				}
			}*/
		}
	}

	void OnTriggerExit(Collider col2){
		if (col2.tag == "characters") {
			
			inTrigger.Remove (col2.transform.gameObject);

			/*for (int i = 0; i < inTrigger.Count; i++) {
				if (inTrigger [i] == col2.gameObject) {
					inTrigger [i] = null;
				}
			}*/
		}
	}
}
