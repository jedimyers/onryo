﻿using UnityEngine;
using System.Collections;

public class Animations : MonoBehaviour {
	public Vector3 target;
	public GameObject[] attackClips;
	public Animator animator;
	//public Animation animations;
	public attack attack;
	public EnemyAI enemy;
	public float timer;
	public bool ai;
	public HealthScript healthScript;
	public float staminaCostLight;
	public sound Audio;
	public bool strike;
	public bool heavy;
	public bool light;
	public GameObject bullet;
	public Transform gunSpawn;
	public string faction;
	public GameObject playerBase;
	public Vector3 prev;
    public bool stun;
	public bool lightAttack;
	public bool attacking;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!ai) {

			if (playerBase.transform.position != prev) {
				animator.SetBool ("running", true);
			} else {
				animator.SetBool ("running", false);
			}
			prev = playerBase.transform.position;

			animator.SetBool ("gunout", false);

			if (attack.combat_mode && !attack.gun) {
				
				if (attack.attacking && animator.GetBool ("attack") == false) {
					timer += Time.deltaTime;
					attack.attacking = false;
					animator.SetBool ("attack", true);
					animator.SetBool ("idle", false);
					attacking = true;
					heavy = true;
				} else if (attack.lightAttack && animator.GetBool ("lightAttack") == false) {
					timer += Time.deltaTime;
					attack.lightAttack = false;
					animator.SetBool ("lightAttack", true);
					animator.SetBool ("idle", false);
					lightAttack = true;

				} else {
					heavy = false;
					animator.SetBool ("lightAttack", false);
					animator.SetBool ("attack", false);
					animator.SetBool ("idle", true);
				}
			} else if (attack.combat_mode && attack.gun) {
				animator.SetBool ("gunout", true);

			} 

			if (!attack.combat_mode) {
				animator.SetBool ("initiatedCut", false);
			}

			if (attack.gunshot) {
				attack.gunshot = false;
				timer += Time.deltaTime;
				//animator.SetTrigger ("gunshot");
				animator.SetBool ("firing", true);
			}
		} else {

			if (playerBase.transform.position == prev && enemy.inRange) {
				animator.SetBool ("running", false);
				//animator.SetBool("idle", true);
				animator.SetBool ("inRange", true);
			} 

            if (playerBase.transform.position != prev && enemy.inRange == false)
            {
                animator.SetBool("running", true);
                animator.SetBool("idle", false);
			}else if (playerBase.transform.position == prev && !enemy.inRange) {
				animator.SetBool ("running", false);
			}


            if (stun)
            {
                Debug.Log("stun");
                animator.SetTrigger("stun");
                stun = false;
                animator.SetBool("attack", false);
                animator.SetBool("idle", false);
                animator.SetBool("running", false);
            }

            if (enemy.hostile)
            {
                if (enemy.attacking)
                {
                    timer += Time.deltaTime;
                    animator.SetBool("attack", true);
                    animator.SetBool("idle", false);
                    enemy.transform.Translate(Vector3.forward * 3f * Time.deltaTime);
                }
                else
                {
                    animator.SetBool("attack", false);


                }
                
            }
            else {

                animator.SetBool("idle", true);
                animator.SetBool("attack", false);
                animator.SetBool("inRange", false);
            }
			prev = playerBase.transform.position;
        
		}
	}

	void setIdle(){
		if (!ai) {
			if (lightAttack) {
				lightAttack = false;
				healthScript.stamina -= (staminaCostLight);
			} else if (attacking) {
				attacking = false;
				healthScript.stamina -= (staminaCostLight*3f);
			}
			attack.attacking = false;
			attack.lightAttack = false;
			animator.SetBool ("idle", true);
			animator.SetBool ("attack", false);
			animator.SetBool ("lightAttack", false);
		} else {
			enemy.attacking = false;
			healthScript.stamina -= staminaCostLight;
		}

        

	}

    public void stunRemove() {
        stun = false;

    }

	public void strikeHit(){
		//Audio.audioSource.clip = Audio.hit;
		//Audio.audioSource.Play();
		//animator.SetBool ("attack", false);
		//animator.SetBool ("lightAttack", false);
		if (strike) {
			
			Audio.hitSound ();
		} else {
            Audio.missSound();
		}
		//Audio.hitSound();
	}

	public void strikeFalse(){
		strike = false;
	}

	void setInitiation(){
		if (!ai) {
			attack.lightAttack = false;
            animator.SetBool("lightAttack", false);
        }
        
        animator.SetBool ("attack", false);
		animator.SetBool ("initiatedCut", true);
	}

	public void firing(){
		attack.gunshot = false;
		animator.SetBool ("firing", false);
	}

	public void bulletSpawn(){
		Audio.gunSound ();
		GameObject gunshot;
		gunshot = Instantiate (bullet, new Vector3 (gunSpawn.transform.position.x, gunSpawn.transform.position.y, gunSpawn.transform.position.z), gunSpawn.transform.rotation);
		gunshot.GetComponent<bulletScript> ().faction = faction;
		attack.bulletCount -= 1;
		//gunshot.transform.Translate (Vector3.forward * 10f * Time.deltaTime);
	}

	public void lung(){
		target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		target = new Vector3(target.x,playerBase.transform.position.y,target.z);
		playerBase.transform.position = Vector3.MoveTowards (playerBase.transform.position, target, 30f * Time.deltaTime);
	}
}
