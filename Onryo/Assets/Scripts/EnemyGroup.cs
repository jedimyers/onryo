﻿using UnityEngine;
using System.Collections;

public class EnemyGroup : MonoBehaviour {
	public UnityEngine.AI.NavMeshAgent nav;
	public Transform[] waypoint;        // The amount of Waypoint you want
	public float patrolSpeed = 3f;  
	public Transform target;
	public float health;
	public float damage;
	public bool attacking;
	float timer;
	public bool hostile;
	public bool moving;
	public bool future_enemy;

	public float pauseDuration = 0;   
	private float curTime;
	private int currentWaypoint = 0;
	private CharacterController character;
	public Vector3 moveDirection;
	public bool hostilityTimer;
	public float timerTrigger;
	public bool loop;


	// Use this for initialization
	void Start () {
		nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
	}

	// Update is called once per frame
	void Update () {
		if (moving) {
			if (currentWaypoint < waypoint.Length) {
				patrol ();
			} else {    
				if (loop) {
					currentWaypoint = 0;
				} 
			}
		}

		if (hostilityTimer) {
			timerTrigger += Time.deltaTime;
		}

		if (timerTrigger >= 5.0f) {
			hostile = true;
		}
			

	}

	public void damaging(float hurt){
		health -= hurt;
	}


	public void turnHostile(){
		hostilityTimer = true;
	}

	public void patrol(){
		target = waypoint [currentWaypoint].transform;
		 // Keep waypoint at character's height

		moveDirection = target.position - transform.position;

		if (moveDirection.magnitude < 0.5f) {
			if (curTime == 0)
				curTime = Time.time; // Pause over the Waypoint
			if ((Time.time - curTime) >= pauseDuration) {
				currentWaypoint++;
				curTime = 0;
			}
		} else {        
			//nav.SetDestination (target);
			transform.LookAt(target);
			transform.Translate (transform.forward * patrolSpeed * Time.deltaTime);
		}
	}
}

