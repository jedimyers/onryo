﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FactionAI : MonoBehaviour {

	public string faction;
	public List<string> rivals;
	public List<nodeAI> node;
    public List<gangAI> gangs;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void gangFactionChange()
    {
        int a;
        for (a = 0; a < gangs.Count; a++) {
            gangs[a].rivals = rivals;
        }

    }
}
