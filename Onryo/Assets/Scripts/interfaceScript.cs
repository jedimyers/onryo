﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class interfaceScript : MonoBehaviour {

	public HealthScript health;
	public Slider healthbar;
	public Slider staminabar;
    public Image red;
    public bool redScreenFlash;
    public float timer;
    public float previousHealth;
    float change;
    public Color b;
	public attack attack;
	public GameObject[] bullets;
	public GameObject portrait;

    // Use this for initialization
    void Start () {
		healthbar.maxValue = health.health;
		staminabar.maxValue = health.stamina;
        previousHealth = health.health;
        red = red.GetComponent<Image>();
        b = red.color;
        Color c = red.color;
        c.a = 0;
        red.color = c;
    }
	
	// Update is called once per frame
	void Update () {
		for (int a = 0; a < bullets.Length; a++) {
			if (a > attack.bulletCount-1) {
				bullets [a].SetActive (false);
			}
		}

		if (attack.combat_mode) {
			portrait.SetActive (true);
		} else {
			portrait.SetActive (false);
		}
			

		healthbar.value = health.health;
		staminabar.value = health.stamina;

        if(health.health < previousHealth)
        {

            healthLower();
            previousHealth = health.health;
        }

        if (redScreenFlash)
        {
            timer += Time.deltaTime;
        }

        if (timer < 0.5f && redScreenFlash)
        {

            red.color = b;
        } else if (timer > 0.5f && timer < 3f) {
        
            change -= Time.deltaTime * 2f;
            Color c = red.color;
            c.a = change;
            red.color = c;
        } else {

            redScreenFlash = false;
            timer = 0;
            
        }
        
	}

    public void healthLower() {

        redScreenFlash = true;

    }
}
