﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gangAI : MonoBehaviour {

	public List<EnemyAI> aiCharacters;
	public EnemyAI boss;
	public string currentOrders;
	// 1 - Raid - Go to the nearest enemy node and take it
	// 2 - Patrol - Follow path in local territory
	// 3 - Ambush - Wait for enemy to step on trigger and attack
	// 4 - Guard - Wait by area patrol point
	public nodeAI nodeGuard;
	public bool occupied;
	public bool hostile;
    public List<string> rivals;
    public FactionAI faction;
    public GameObject target;
    public bool guarding;


    private void Awake()
    {
		
        if (rivals.Count == 0)
        {
          //  rivals = faction.rivals;
        }
    }

    // Use this for initialization
    void Start () {
        if (currentOrders == "guard")
        {
            if (!hostile)
            {
                assemble();
                guarding = true;
            }
        }
        else if (currentOrders == "move")
        {
            if (!hostile)
            {
                move();
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (currentOrders == "guard")
        {
            if (!hostile)
            {
                if (!guarding)
                {
                    assemble();
                }
            }
        }
        else if (currentOrders == "move")
        {
            if (!hostile)
            {
                move();
                guarding = false;
            }
        }


        if (hostile)
        {
            
            attackCharacter();
            int a;
            for (a = 0; a < aiCharacters.Count; a++)
            {
                if (aiCharacters[a] != null)
                {
                    attackCharacter();
                    aiCharacters[a].hostile = true;
                    aiCharacters[a].moving = false;
                }
            }
        }
        else {

           // reportChange();

        }
	}

    public void attackCharacter()
    {
        int a;
        for (a = 0; a < aiCharacters.Count; a++)
        {
            if (aiCharacters[a].target == null)
            {
                aiCharacters[a].target = target.transform.position;
            }
        }
    }

    public void targeting (GameObject targ) {
        hostile = true;

        target = targ;
    }


    public void assemble(){
        if (!hostile)
        {
            if (boss != null && boss.nav.enabled == true)
            {
                boss.nav.SetDestination(nodeGuard.bosspoint.position);
            }

            int a;
            for (a = 0; a < aiCharacters.Count; a++)
            {
                if (aiCharacters[a] != null && aiCharacters[a].nav.enabled == true)
                {
                    aiCharacters[a].nav.SetDestination(nodeGuard.waypoint[a].position);
                    //aiCharacters[a].moving = false;
                }
            }
        }
	}

	public void move(){
        if (!hostile)
        {
            if (boss != null)
            {
                boss.moving = true;
            }

            int a;
            for (a = 0; a < aiCharacters.Count; a++)
            {
                if (aiCharacters[a] != null)
                {
                    aiCharacters[a].moving = true;
                }
            }
        }
	}

    public void reportChange() {
        faction.rivals = rivals;
        faction.gangFactionChange();
    }

}
