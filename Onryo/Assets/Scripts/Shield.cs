﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {

	public Transform defended;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position = defended.transform.position + defended.transform.forward;
		transform.localRotation = defended.transform.localRotation;
	}
}
