﻿using UnityEngine;
using System.Collections;

public class MoveDirection : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray, out hit)) {
			//nav.SetDestination (hit.point);
			transform.position = hit.transform.position;
		}

	}
}
