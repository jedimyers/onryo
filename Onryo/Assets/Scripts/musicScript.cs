﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicScript : MonoBehaviour {

	public AudioClip[] musicTracks;
	public AudioSource audioSource;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void startTrack(int a){
		Debug.Log (a);
		audioSource.clip = musicTracks [a];
		audioSource.Play ();
	
	}
}
