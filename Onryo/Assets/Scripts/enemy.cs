﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class enemy : MonoBehaviour {
	public UnityEngine.AI.NavMeshAgent nav;
	public Transform[] waypoint;        // The amount of Waypoint you want
	public float patrolSpeed = 3f;  
	public GameObject target;
	public float health;
	public float damage;
	public bool attacking;
	public float timer;
	public bool hostile;
	public bool moving;
	public bool future_enemy;

	public float pauseDuration = 0;   
	private float curTime;
	private int currentWaypoint = 0;
	private CharacterController character;
	public Vector3 moveDirection;
	public bool hostilityTimer;
	public float timerTrigger;
	public bool loop;
	public string faction;

	public bool gangfight;

	public string enemyGang;
	public bool possessed;

	public Slider health_bar;


	// Use this for initialization
	void Start () {
		nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
		health_bar.maxValue = health;
	}
	
	// Update is called once per frame
	void Update () {


		health_bar.value = health;

		if (hostilityTimer) {
			timerTrigger += Time.deltaTime;
		}

		if (timerTrigger >= 5.0f) {
			hostile = true;
		}

		if (hostile) {
			if (FindClosestEnemy () != null) {
				nav.destination = FindClosestEnemy ().transform.position;
			}

			Vector3 fwd = transform.TransformDirection (Vector3.forward);
			RaycastHit hit;

			//if(Physics.Raycast(transform.position, transform.forward, out hit, 5.0f)){
			if (Physics.SphereCast(transform.position, 0.1f, transform.forward, out hit, 5.0f)) {
				Debug.Log (hit.transform.gameObject);

				if (hit.transform.tag == "pcs") {
					attacking = true;
					nav.Stop ();
				}

				if (attacking == true) {
					timer += Time.deltaTime;

					if (timer >= 0.35) {
						hit.transform.gameObject.GetComponent<HealthScript> ().health -= 1.0f;
						timer = 0;
						attacking = false;
					}


				} else {
					timer = 0;
				}
					

			} else {
				nav.Resume ();
				nav.destination = FindClosestEnemy ().transform.position;
			}
		
		} else if (moving) {
			if(currentWaypoint < waypoint.Length){
				patrol();
			}else{    
				if(loop){
					currentWaypoint=0;
				} 
			}

		} 
			
	}

	public void damaging(float hurt){
		health -= hurt;
	}

	GameObject FindClosestEnemy() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("pcs");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance && !go.GetComponent<HealthScript>().deathTime) {
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}

	GameObject FindClosestGang() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag(enemyGang);
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			//if (gang != go.GetComponent<enemy>().gang) {
				if (curDistance < distance) {
					closest = go;
					distance = curDistance;
					go.GetComponent<enemy> ().gangfight = true;

				}
			//}
		}
		return closest;
	}


	public void turnHostile(){
		hostilityTimer = true;
	}

	public void gangFight(){
		gangfight = true;
	}

	public void patrol(){
		Vector3 target = waypoint [currentWaypoint].position;
		target.y = transform.position.y; // Keep waypoint at character's height

		moveDirection = target - transform.position;

		if (moveDirection.magnitude < 0.5f) {
			if (curTime == 0)
				curTime = Time.time; // Pause over the Waypoint
			if ((Time.time - curTime) >= pauseDuration) {
				currentWaypoint++;
				curTime = 0;
			}
		} else {        
			nav.SetDestination (target);
		}
	}
}
