﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour {

	public string faction;
	public float speed;
	public GameObject bloodspurt;
	public cameraShake camShake;

	// Use this for initialization
	void Awake () {
		camShake = Camera.main.GetComponent<cameraShake> ();
		camShake.DoShake ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (transform.forward * speed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider hit){
		if (hit.transform.tag == "characters") {
			hit.GetComponent<HealthScript> ().health = hit.GetComponent<HealthScript> ().health - 2f;
            //hit.GetComponent<EnemyAI> ().factionAI.rivals.Add (faction);
            gangAI fact = hit.GetComponent<EnemyAI>().gangs;
            bool b = false;
            int a;
            for (a = 0; a < fact.rivals.Count; a++)
            {

                if (fact.rivals[a] == faction)
                {
                    b = true;
                    //factionRival = true;

                }
                else
                {
                    b = false;
                }

                if (a >= fact.rivals.Count - 1)
                {
                    if (b == false)
                    {
                        fact.rivals.Add(faction);
                    }
                }
            }
			hit.GetComponent<EnemyAI>().stun = true;
            GameObject blooda;
			blooda = Instantiate (bloodspurt, new Vector3 (hit.transform.position.x, hit.transform.position.y-0.2f, hit.transform.position.z), bloodspurt.transform.rotation);
			blooda.transform.Rotate (blooda.transform.rotation.x, blooda.transform.rotation.y-0.9f, Random.Range(0f, 360f));
			camShake.DoShake ();
			Destroy (this.gameObject);
		} else if (hit.transform.tag == "node"){
			
			Destroy (this.gameObject);
		}
	}
}
