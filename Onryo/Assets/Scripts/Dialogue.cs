﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class Dialogue : MonoBehaviour {

	public string[] dialogueList;
	public int dialogueA;
	public int dialogueButtonB;
	public GameObject[] buttons;
	public Text dialoguePrompt;
	public Text[] dialogueButtons;
	public GameObject dialogue;
	public GameObject continueButton;
	public dialogueOperator dialogueOp;

	// Use this for initialization
	void Awake () {
		dialoguePrompt = GameObject.FindGameObjectWithTag ("dialogue").GetComponent<Text> ();
		dialogue = GameObject.FindGameObjectWithTag ("dialogueText");
		dialogue.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (dialogue != null) {
			dialoguePrompt.text = dialogueList [dialogueA];
			buttons [dialogueButtonB].SetActive (true);
		}
			
	}

	void OnTriggerStay(Collider col){
		if(col.tag == "characters"){
			if (Input.GetKeyDown (KeyCode.E)) {
				dialogue.SetActive (true);
			}
		}
	}
}
