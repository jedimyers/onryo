﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorList : MonoBehaviour {

	public List<GameObject> sensorlist;
	public GameObject[] sensors;

	// Use this for initialization
	void Start () {
		sensors = GameObject.FindGameObjectsWithTag ("sensors");
		int a;
		for (a = 0; a < sensors.Length; a++) {
			sensorlist.Add (sensors [a]);
		}
	}

	// Update is called once per frame
	void Update () {

	}

	public void removeFromSensors(GameObject a){
		Debug.Log ("triggers");
		sensorlist.Remove (a);
	}	

	public void removeFromInTriggers(GameObject a){
		int d;
		for (d = 0; d < sensors.Length; d++){
			sensorlist [d].GetComponent<sensor> ().inTrigger.Remove (a.gameObject);

		}
	}
}
