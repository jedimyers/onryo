﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

	public float attackSpeed;
	public float defense;
	public float damage;
	public float immunities;
	public float speed;
	public weapon weapon;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		attackSpeed = weapon.speed;
		damage = weapon.damage;
	}
		

}
