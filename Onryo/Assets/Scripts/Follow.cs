﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

	public Transform follow;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(follow != null)
		transform.position = follow.position;
	}
}
