﻿using UnityEngine;
using System.Collections;

public class HealthScript : MonoBehaviour {

    public bool boss;
	public float health;
    public float lastHealth;
	public float stamina;
	public float maxStamina;
	public float staminaRegen;
	public Animator anim;
	public GameObject[] corpse;
	public SensorList sensor;
	public sensor thisSensor;
	public bool spawnDead;
	public bool player;
	public GameLogic gameLogic;
	public bool deathTime;
	public float deathTimer;
    public GameLogic game;
    public uishake shake;
    public int i;
    // Use this for initialization

    void Start () {
        game = GameObject.Find("GameLogic").gameObject.GetComponent<GameLogic>();
        i = Random.Range(0, 2);
    }
	
	// Update is called once per frame
	void Update () {

        if (lastHealth > health && player) {
            shake.shake();
        }

        lastHealth = health;

		if (health <= 0) {

			deathTimer += Time.deltaTime;

			if (deathTime == true) {
				gameLogic.deathTime = true;
				deathTime = false;
			}

			if (!spawnDead) {
				spawnDead = true;
				GameObject corpsea;

				corpsea = Instantiate (corpse[i], new Vector3 (transform.position.x, (Random.Range(0.2f, 0.3f)), transform.position.z), corpse[i].transform.rotation);
				corpsea.transform.Rotate (corpse[i].transform.rotation.x, corpse[i].transform.rotation.y, Random.Range (0f, 360f));
				this.GetComponent<Collider> ().enabled = false;

				anim.gameObject.SetActive (false);
                if (!player)
                {
                    if (boss)
                    {
                        this.GetComponent<EnemyAI>().gangs.boss = null;
                    }
                    else
                    {
                        this.GetComponent<EnemyAI>().gangs.aiCharacters.Remove(this.GetComponent<EnemyAI>());
                    }
                    game.allSpawned.Remove(this.gameObject);
                }
			}
			//sensor.removeFromInTriggers(this.gameObject);
			//sensor.removeFromSensors (thisSensor.gameObject);



			if (deathTimer >= 1f) {

                if (player)
                {

                    gameLogic.restart();

                }
                else {
               
                    Destroy(this.gameObject);
                }
			}


			//this.gameObject.SetActive (false);



		}

		if (stamina < maxStamina) {
			stamina += staminaRegen * Time.deltaTime;
		} 

		/*if (stamina < 0f) {
		
			stamina = 0;
		}*/
	}
}
