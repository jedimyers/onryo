﻿using UnityEngine;
using System.Collections;

public class facing : MonoBehaviour {
	public float angle;
	public Vector3 g;
	public attack attack;
	public Transform transformMoving;
	public float speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        

		if (!attack.lightAttack && !attack.attacking) {
			Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
			Vector3 dir = Input.mousePosition - pos;
			angle = Mathf.Atan2 (dir.x, dir.y) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis (angle, Vector3.up);
			transform.rotation = Quaternion.Lerp(transform.rotation, transformMoving.rotation, Time.deltaTime * speed);
		} else {
			Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
			Vector3 dir = Input.mousePosition - pos;
			angle = Mathf.Atan2 (dir.x, dir.y) * Mathf.Rad2Deg;
			transformMoving.rotation = Quaternion.AngleAxis (angle, Vector3.up);
			transform.rotation = Quaternion.Lerp(transform.rotation, transformMoving.rotation, Time.deltaTime * speed);
		}
	}
}
