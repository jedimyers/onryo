﻿using UnityEngine;
using System.Collections;

public class trigger_attacl : MonoBehaviour {

	public ScriptTriggers scripts;
	public float timer;

	// Use this for initialization
	public void OnTriggerEnter(Collider col){
		if (col.transform.tag == "enemy") {
			if (col.transform.GetComponent<enemy> ().future_enemy == true) {
				timer += Time.deltaTime;
				col.transform.GetComponent<enemy> ().turnHostile ();
				scripts.ActivateCivilians ();
				if(timer >= 2.0f){ 
					this.gameObject.SetActive (false);
				}
			}
		}
	}
}
