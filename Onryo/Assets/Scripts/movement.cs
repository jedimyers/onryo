﻿using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {

	public bool selected;
	public UnityEngine.AI.NavMeshAgent nav;
	public GameObject target;
	public attack attack;
	public float speed;
	public float baseSpeed;
	public bool forwardkey;
	public bool backKey;
	public bool leftKey;
	public bool rightKey;
	public HealthScript health;
	public float timer;
    public bool timerActive;
    public GameObject trail;
	public Collider box;
    public Vector3 jumpDirection;


	// Use this for initialization
	void Start () {

		//attack = GetComponent<attack> ();

	}
	
	// Update is called once per frame
	void Update () {

		GetComponent<Rigidbody> ().velocity = Vector3.zero;
		Debug.Log(attack.attacking && attack.lightAttack);
		if (attack.canMove == true) {
			if (Input.GetKey (KeyCode.W)) {
				forwardkey = true;
				if (Input.GetKeyDown (KeyCode.Space)) {
					if ((!backKey && !leftKey && !rightKey) && health.stamina >= 1f) {
                    timer = 0;
                    health.stamina -= 1f;
                        trail.SetActive(true);
                        timerActive = true;
                    jumpDirection = Vector3.forward;
                }
				} else {
					transform.Translate (Vector3.forward * speed * Time.deltaTime);
				}
            }

			if (attack.WrathMode) {
				speed = baseSpeed * 4;
			} else {
				speed = baseSpeed;
			}

			if (Input.GetKey (KeyCode.A)) {
				leftKey = true;
				if (Input.GetKeyDown (KeyCode.Space)) {
					if ((!forwardkey && !rightKey && !backKey) && health.stamina >= 1f) {
                    timer = 0;
					health.stamina -= 1f;
                    trail.SetActive(true);
                    timerActive = true;
                    jumpDirection = -Vector3.right;
                }


            } else {
					transform.Translate (-Vector3.right * speed * Time.deltaTime);
				}
			}
			

			if (Input.GetKey (KeyCode.S)) {
				backKey = true;
				if (Input.GetKeyUp (KeyCode.Space)) {
					if ((!forwardkey && !leftKey && !rightKey) && health.stamina >= 1f) {
                    timer = 0;
                    health.stamina -= 1f;
                        trail.SetActive(true);
                        timerActive = true;
                    jumpDirection = -Vector3.forward;
                }

            } else {
					transform.Translate (-Vector3.forward * speed * Time.deltaTime);
				}
			}

			if (Input.GetKey (KeyCode.D)) {
				rightKey = true;
				if (Input.GetKeyDown (KeyCode.Space)) {
					if ((!forwardkey && !leftKey && !backKey) && health.stamina >= 1f) {
                    timer = 0;
                    health.stamina -= 1f;
                    trail.SetActive(true);
                    timerActive = true;
                    jumpDirection = Vector3.right;
                }
                    
            } else {
					transform.Translate (Vector3.right * speed * Time.deltaTime);						
				}
			}

            if (timerActive) {
                timer += Time.deltaTime;
                transform.tag = "invincible";
                trail.SetActive(true);
                transform.Translate(jumpDirection * speed * Time.deltaTime * 10f);
            }

			if (timer >= 0.10f)
			{
				GetComponent<Collider>().enabled = true;
				timer = 0;
                transform.tag = "characters";
                trail.SetActive(false);
				timerActive = false;
			}


        if (Input.GetKeyUp (KeyCode.W)) {
				forwardkey = false;
			}

			if (Input.GetKeyUp (KeyCode.A)) {
				leftKey = false;
			}
		
			if (Input.GetKeyUp (KeyCode.S)) {
				backKey = false;
			}
		
			if (Input.GetKeyUp (KeyCode.D)) {
				rightKey = false;
			}
		}
	}

	//public void OnMouseDown() {
	//	selected = !selected;
	//}
}


