﻿using UnityEngine;
using System.Collections;

public class WapointSystem : MonoBehaviour {

	public UnityEngine.AI.NavMeshAgent agent;
	public Transform[] waypoint;        // The amount of Waypoint you want
	public float patrolSpeed = 3f;       // The walking speed between Waypoints
	public bool  loop = true;       // Do you want to keep repeating the Waypoints
	public float dampingLook= 6.0f;          // How slowly to turn
	public float pauseDuration = 0;   // How long to pause at a Waypoint

	private float curTime;
	private int currentWaypoint = 0;
	private CharacterController character;
	public Vector3 moveDirection;

	public Transform flee_destination;
	public bool fleeing;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		RaycastHit hit;

		if(Physics.SphereCast(transform.position, 1.0f, transform.forward, out hit, 8.0f)){
			Debug.Log (hit);
			if(hit.transform.tag == "Player"){
				Debug.Log("Attack");
			}
		}


		if (fleeing == false) {
			if (currentWaypoint < waypoint.Length) {
				patrol ();
			} else {    
				if (loop) {
					currentWaypoint = 0;
				} 
			}
		} else {
			agent.SetDestination (flee_destination.position);
		}


	}

	void  patrol (){

		Vector3 target = waypoint[currentWaypoint].position;
		target.y = transform.position.y; // Keep waypoint at character's height

		moveDirection = target - transform.position;

		if(moveDirection.magnitude < 0.5f){
			if (curTime == 0)
				curTime = Time.time; // Pause over the Waypoint
			if ((Time.time - curTime) >= pauseDuration){
				currentWaypoint++;
				curTime = 0;
			}
		}else{        
			agent.SetDestination (target);
		}  
	}

	public void fleeFrom (){
		fleeing = true;
	}
}
