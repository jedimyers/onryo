﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class attack : MonoBehaviour {

	public RaycastHit hit;
	public RaycastHit[] hits;
	public Vector3 target;
	public movement move;
	public float health;
	public bool attacking;
	public bool lightAttack;
	public float timer;
	public bool blocking;
	public GameObject shield;
	public float attackTimer;
	public SwordStrike strike;
	public GameObject wrathcam;
	public float wrathtime;
	public GameObject maincam;
	public bool combat_mode;
	public float attackSpeed;
	public Stats stats;
	public DamageScript damage;
	public HealthScript healthScript;
	public float staminaCostLight;
	public musicScript music;
	public facing facing;
	public GameObject playerBase;
	public bool gun;
	public bool gunshot;
	public int bulletCount;
	public Text bullets;
	public string bulletText;
	public float gunTimer;
    public int i;
	public bool canMove;
    public bool WrathMode;
	// Use this for initialization

	void Start () {
		move = GetComponent<movement>();
		bulletCount = 6;
		music.startTrack (Random.Range (0, 2));
        i = Random.RandomRange(0, 2);
    }
	
	// Update is called once per frame
	void Update ()
	{
		if (combat_mode) {

			if (canMove == false) {
				timer += Time.deltaTime;
				if (timer >= 0.5f) {
					timer = 0;
					canMove = true;
					playerBase.GetComponent<Collider> ().enabled = true;
				} else {
					playerBase.GetComponent<Collider> ().enabled = false;

				}
			}

			if (Input.GetKeyUp(KeyCode.F)) {
				if(combat_mode == true)
					combat_mode = false;
			}

			//bulletText = "Bullets: " + bulletCount.ToString (); 
			//bullets.text = bulletText;

			if (Input.GetMouseButtonDown (2) && !attacking) {
				if (!combat_mode) {
					combat_mode = true;
					music.startTrack (i);
				}
				gun = true;
				attacking = false;
				lightAttack = false;
			} else if (Input.GetMouseButton (2)) {
				gunTimer += Time.deltaTime;
			} else if (Input.GetMouseButtonUp (2) && gunTimer < 0.5f && bulletCount > 0) {
				gunshot = true;
				gunTimer = 0;
				gun = false;
			} else if (Input.GetMouseButtonUp (2) && gunTimer >= 0.5f && bulletCount > 0) {
				gunTimer = 0;
				gun = false;
				gunshot = false;
			}


			if (gun && bulletCount > 0) {
				if (Input.GetMouseButtonDown (0)) {
				
					gunshot = true;
					if (!combat_mode) {
						combat_mode = true;
						music.startTrack (1);
					}
					//timer += Time.deltaTime;

				} 


			} else if (bulletCount <= 0) {
				gun = false;
				gunshot = false;
			}

			if (blocking == false && gun == false) {
				if (healthScript.stamina > 0.2f && !combat_mode) {
					if (Input.GetMouseButtonDown (1)) {
						lightAttack = true;
						canMove = false;
						if (!combat_mode) {
							print ("a");
							music.startTrack (0);
						}
					} else if (Input.GetMouseButtonDown (0)) {
						lightAttack = true;
						canMove = false;
						if (!combat_mode) {
							music.startTrack (1);
						}
					} 
				} else if (healthScript.stamina > 0.2f && combat_mode) {
					if (Input.GetMouseButtonDown (1)) {
						attacking = true;
						canMove = false;
						if (!combat_mode) {
							music.startTrack (0);
						}
					} else if (Input.GetMouseButtonDown (0)) {
						lightAttack = true;
						canMove = false;
						if (!combat_mode) {
							music.startTrack (1);
						}
					} 

					if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1)) {

						combat_mode = true;
					}
				}

				

				if (WrathMode) {
					wrathtime += Time.deltaTime;

					if (wrathtime >= 10) {
						WrathMode = false;
						wrathcam.SetActive (false);
						maincam.SetActive (true);
					}
				}
					

			}
		} else if (combat_mode == false){
		
			if (Input.GetKeyUp(KeyCode.F)) {
				if (combat_mode == false) {
					combat_mode = true;
					music.startTrack (Random.Range (0, 2));
				}

			}
		}
	}
}
