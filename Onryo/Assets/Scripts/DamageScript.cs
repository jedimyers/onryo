﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DamageScript : MonoBehaviour {

	public float damage;
	public string enemy;
	public string faction;
	public EnemyAI enemyAI;
	public factions factionAI;
	public GameObject character;
	public bool ai;
	public cameraShake cameraShake;
	public sound Audio;
	public bool strike;
	public Animations anim;
	public GameObject bloodspurt;
	public GameLogic logic;
    public facing facing;


	// Use this for initialization
	void Start () {
		faction = factionAI.faction;
	}

	// Update is called once per frame
	void Update () { 
		faction = factionAI.faction;

		//damage = character.GetComponent<Stats> ().damage;
	}


	public void strikeHit(){
		if (strike) {
			if (Audio.audioSource.isPlaying) {
				Audio.audioSource.clip = Audio.hit;
				Audio.audioSource.Play ();
			}
		}

	}

	void OnTriggerEnter(Collider hit){
		if (hit.transform.tag == enemy) {

			if (!ai) {
				if (hit != null) {
					bool factionRival = false;

                    /*for (int e = 0; e < hit.GetComponent<EnemyAI>().factionAI.rivals.Count; e++) {
						if (hit.GetComponent<EnemyAI>().factionAI.rivals [e] == faction) {
							Debug.Log ("kill");
							factionRival = true;
						}

					}*/

                    if (hit.GetComponent<factions>().faction != faction)
                    {
                        gangAI fact = hit.GetComponent<EnemyAI>().gangs;
                        bool b = false;
                        int a;
                        for (a = 0; a < fact.rivals.Count; a++)
                        {
                           // Debug.Log(fact.rivals[a]);
                            if (fact.rivals[a] == faction)
                            {
                                b = true;
                                
                                //factionRival = true;

                            }
                            else {
                                b = false;
                            }

                            if (a >= fact.rivals.Count-1) {
                                if (b == false)
                                {
                                    fact.rivals.Add(faction);
                                }
                            }
                        }


                        cameraShake.DoShake();
                        //if(hit.GetComponent<HealthScript> ().health < damage){
                        logic.killCount += 1;
                        //}

                        //hit.GetComponent<EnemyAI>().stun = true;

					


                        if (anim.heavy)
                        {
                            hit.GetComponent<HealthScript>().deathTime = true;
                            anim.heavy = false;
                        }

                        hit.GetComponent<HealthScript>().health = hit.GetComponent<HealthScript>().health - damage;
                        
		            	hit.GetComponent<sound> ().hitSound ();
                        GameObject blooda;
						blooda = Instantiate (bloodspurt, new Vector3 (hit.transform.position.x, Random.Range(0.15f, 0.2f), hit.transform.position.z), bloodspurt.transform.rotation);
                        blooda.transform.Rotate(blooda.transform.rotation.x, blooda.transform.rotation.y, Random.Range(0f, 360f));
                    }

				}

			}
			else if (hit.GetComponent<factions>().faction != faction){


				hit.GetComponent<HealthScript> ().health = hit.GetComponent<HealthScript> ().health - damage;
                hit.GetComponent<sound>().hitSound();
                GameObject blooda;
				blooda = Instantiate (bloodspurt, new Vector3 (hit.transform.position.x, Random.Range(0.15f, 0.2f), hit.transform.position.z), bloodspurt.transform.rotation);
				blooda.transform.Rotate (blooda.transform.rotation.x, blooda.transform.rotation.y, Random.Range(0f, 360f));

			}
		}
	}
}
