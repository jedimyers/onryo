﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uishake : MonoBehaviour {

    public RectTransform uiShaker;
    public Vector3 origin;

    public float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    // Use this for initialization
    void Start () {
        origin = uiShaker.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        if (shakeDuration > 0)
        {
            uiShaker.localPosition = origin + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            uiShaker.localPosition = origin;
        }
    }

    public void shake() {
        shakeDuration = 2f;
    }

}
