﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound : MonoBehaviour {

	public AudioSource audioSource;
	public AudioSource missSource;
	public AudioClip hit;
	public AudioClip[] swordcut;
	public AudioClip[] boom;
	public AudioClip[] miss; 
	public AudioClip[] pull;
	public Animations anim;
    public int i;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        i = Random.Range(0, 2);

    }

	public void hitSound(){

        //if (anim.strike) {
        
        audioSource.clip = swordcut[i];
        audioSource.pitch = Random.Range(0.8f, 1f);
        audioSource.Play ();
		//}

		if (!missSource.isPlaying) {
			missSource.clip = miss [0];
			missSource.Play ();
		}
	}

	public void gunSound(){


		audioSource.clip = boom[0];
        audioSource.pitch = Random.Range(0.4f, 1f);
        audioSource.Play();


    }

	public void swordcutSound(){

	}

	public void pullSound(){
		audioSource.clip = pull [0];
		audioSource.Play ();
	}

	public void missSound(){
        if (!missSource.isPlaying)
        {
            missSource.clip = miss[0];
            missSource.Play();
        }
    }
}
