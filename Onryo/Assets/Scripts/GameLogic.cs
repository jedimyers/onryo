﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

public class GameLogic : MonoBehaviour {

	public float spawnTimer;
	public float miniBossTimers;
	public bool spawnerActive;
	public Transform[] spawners; 
	public GameObject spawned;
	public GameObject miniBoss;
	public int killCount;
	public Text Counter;
	public Text Tutorial;
	public GameObject box;
	public string[] tutorialText;
	public HealthScript playerHealth;
	public float deathTimer;
	public bool deathTime;
	public bool mainMap;
	public string sceneName;
    public List<GameObject> allSpawned;
	public gangAI gang;

	// Use this for initialization
	void Start () {
		
		Time.timeScale = 1F;
		tutorialText [0] = string.Concat("W - Up \nS - Down \nA - Left \nD - Right \nChange Facing - Move Mouse Cursor \nLeft-Click - Fast Attack/Fire Gun \nRight-Click - Heavy Attack \nHold Middle Mouse - Draw Gun \nTap Middle Mouse - Quick Fire \nSpace + Direction - Allows you to dodge in a direction ");
		tutorialText [1] = "Attacks and dodges require stamina.  Stamina is shown by the green bar.  Using too much stamina will exhaust you until you recover.";
		Tutorial.text = tutorialText [0];
		//Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {
		Application.targetFrameRate = 60;
		//Debug.Log (1.0f / Time.smoothDeltaTime);
		if (deathTime) {
			deathTimer += Time.deltaTime;
			if (deathTimer >= 0.10f) {
				deathTimer = 0;
				Time.timeScale = 1F;
				deathTime = false;
			} else {
				Time.timeScale = 0.25F;
			}
		}
	
		if (spawnerActive) {
			spawnTimer += Time.deltaTime;
			miniBossTimers += Time.deltaTime;
			Counter.text = "Kill Count: " + killCount.ToString ();
            if (allSpawned.Count < 10)
            {
                if (spawnTimer > 10)
                {

                    int a;
                    int b = Random.Range(3, 5);

                    for (a = 0; a < b; a++)
                    {
                        int rando = Random.Range(0, 4);
                        GameObject g;
                        g = Instantiate(spawned, new Vector3(spawners[rando].transform.position.x, spawners[rando].transform.position.y - 0.2f, spawners[rando].transform.position.z), spawners[rando].transform.rotation);
                        g.GetComponent<EnemyAI>().moving = true;
                        allSpawned.Add(g.gameObject);
						gang.aiCharacters.Add(g.GetComponent<EnemyAI>());
						g.GetComponent<EnemyAI> ().gangs = gang;
                    }
                    spawnTimer = 0;

                }
                else if (miniBossTimers > 25)
                {

                    int rando = Random.Range(0, 4);
                    GameObject g;
                    g = Instantiate(miniBoss, new Vector3(spawners[rando].transform.position.x, spawners[rando].transform.position.y - 0.2f, spawners[rando].transform.position.z), spawners[rando].transform.rotation);
                    g.GetComponent<EnemyAI>().moving = true;
                    allSpawned.Add(g.gameObject);
					gang.aiCharacters.Add(g.GetComponent<EnemyAI>());
					g.GetComponent<EnemyAI> ().gangs = gang;
                    miniBossTimers = 0;

                }
            }

			if (Input.GetKeyDown (KeyCode.Escape)) {
				box.SetActive (false);
			}

			if (playerHealth.stamina < 2) {
				Tutorial.text = tutorialText [1];
			}
		
		}


	}

	public void restart(){
		Time.timeScale = 1F;

		if (mainMap) {
			SceneManager.LoadScene ("onryo");
		} else {
			SceneManager.LoadScene ("testArena");
		}
	
	}

	public void deactivate(GameObject obj){
		obj.SetActive (false);
	}

	public void activate(GameObject obj){
		obj.SetActive (true);
	}
}
