﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerAI : MonoBehaviour {

	public FactionAI faction;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col){
		if (col.tag == "characters" && col.GetComponent<factions> ().faction == "Player") {
			faction.rivals.Add ("Player");
			faction.gangFactionChange ();
		}
	}
}
