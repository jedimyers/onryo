﻿using UnityEngine;

namespace Dialog.ScriptableObjects {
	[CreateAssetMenu(menuName = "Dialog: Create Dialog")]

	public class Dialogs : ScriptableObject {
		public bool HasCloseButton = true;
		public Lines[] Lines = new Lines[0];
	}
}