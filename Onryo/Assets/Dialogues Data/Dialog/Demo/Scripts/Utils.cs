using UnityEngine;

namespace Demo {
    public class Utils {
        public static Vector3 GetRandomVector3(float minX, float maxX, float minY, float maxY, float minZ, float maxZ) {
            float x = (minX == maxX) ? minX : Random.Range(minX, maxX);
            float y = (minY == maxY) ? minY : Random.Range(minY, maxY);
            float z = (minZ == maxZ) ? minZ : Random.Range(minZ, maxZ);
            return new Vector3(x, y, z);
        }
    }
}