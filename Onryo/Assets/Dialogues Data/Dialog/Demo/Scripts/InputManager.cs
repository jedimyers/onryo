﻿using System.Collections;
using System.Collections.Generic;
using Dialog;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

namespace Demo {

	public class InputManager : MonoBehaviour {

		Transform playerTransform;
		public GameObject player;
		NavMeshAgent playerAgent;
		RaycastHit rayHit;
		BaseNPCBehaviour currentNPC;

		Coroutine waitingCoroutine = null;

		void Start() {
			
			playerTransform = player.transform;
			playerAgent = player.GetComponent<NavMeshAgent>();
		}

		void Update() {
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {
				HandleLeftClick();
			}
#else
			if (
				Input.GetMouseButtonDown(0) &&
				!EventSystem.current.IsPointerOverGameObject()
			) {
				HandleLeftClick();
			}
#endif
		}

		void HandleLeftClick() {
			if (waitingCoroutine != null) {
				StopCoroutine(waitingCoroutine);
			}
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out rayHit, 1000) && rayHit.transform != playerTransform) {
				BaseNPCBehaviour npcBehaviour = rayHit.collider.transform.GetComponent<BaseNPCBehaviour>();
				if (npcBehaviour) {
					currentNPC = npcBehaviour;
					if ((playerTransform.position - currentNPC.transform.position).magnitude < currentNPC.MinDistance) {
						currentNPC.OpenDialog(playerTransform);
					} else {
						waitingCoroutine = StartCoroutine(WaitForPlayerToComeNearby());
					}
				} else {
				}
			}
		}

		IEnumerator WaitForPlayerToComeNearby() {
			yield return new WaitUntil(PlayerIsNearby);
			currentNPC.OpenDialog(playerTransform);
		}

		bool PlayerIsNearby() {
			if ((playerTransform.position - currentNPC.transform.position).magnitude < currentNPC.MinDistance) {
				return true;
			}
			return false;
		}
	}
}