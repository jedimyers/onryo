﻿using UnityEngine;

namespace Demo {
	public class CameraManager : MonoBehaviour {

		Transform playerTransform;
		Vector3 cameraVelocity = Vector3.zero;

		void Start() {
			playerTransform = GameManager.Player.transform;
		}

		void LateUpdate() {
			transform.position = Vector3.SmoothDamp(playerTransform.position, transform.position, ref cameraVelocity, Time.deltaTime * 0.5f);
		}
	}
}