﻿using System.Collections;
using System.Collections.Generic;
using Dialog;
using UnityEngine;

namespace Demo {

    public class TheWhiteOne : BaseNPCBehaviour {

        public GameObject Orb;
        public GameObject Poof;
        public GameObject ExclamationMark;

        GameObject instantiatedPoof;

        public void SpawnOrbs() {
            for (int i = 0; i < 3; i++) {
                Instantiate(Orb, Utils.GetRandomVector3(-9, 9, 0.8f, 0.8f, -9, 9), Quaternion.identity);
            }
        }

        public void PoofOut() {
            ExclamationMark.SetActive(false);
            instantiatedPoof = Instantiate(Poof, transform.position, Quaternion.Euler(-90, 0, 0));
            StartCoroutine(RemoveGo(2.5f));
        }

        IEnumerator RemoveGo(float timeOut) {
            yield return new WaitForSeconds(timeOut);
            CloseDialog();
            Destroy(instantiatedPoof, 0.5f);
            Destroy(gameObject);
            GameManager.Player.GetComponent<BaseNPCBehaviour>().OpenDialog(GameManager.Player.transform, 4);
        }
    }
}