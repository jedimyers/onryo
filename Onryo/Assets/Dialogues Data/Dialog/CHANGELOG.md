# EZDialog Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Mobile Support

- Add tween demo
- Cut Scenes demo

### Fixed
- Fixed bug that timeout coroutine doesnt stop doesnt cancel timeout
- Fixed demo bug that shows wrong dialog on click player
- Fixed bug where close trigger event was called more than once even though line did not change

## [1.0.0] - 2018-01-04
### Added
- All files