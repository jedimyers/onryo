﻿using System.Collections;
using System.Collections.Generic;
using Dialog;
using UnityEngine;

public class peasant : BaseNPCBehaviour {

	bool met;
	public Transform playerTransform;

	// Use this for initialization
	void Start () {
		met = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col){

		if(col.tag == "characters" && col.GetComponent<factions>().faction == "Player"){
			print(col.transform.name.ToString());

			if (!met) {
				OpenDialog (playerTransform, 0);
				met = true;
			}
		}
	}
}
