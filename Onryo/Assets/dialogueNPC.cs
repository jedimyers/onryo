﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueNPC : MonoBehaviour {

	public bool met;
	public bool thing;
	public GameObject currentNPCState;
	public GameObject anim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void aggro(){
		anim.SetActive (true);
		currentNPCState.SetActive (false);
	}

	public void changeTo(GameObject newNPCStrate){
		currentNPCState.SetActive (false);
		currentNPCState = newNPCStrate;
		currentNPCState.SetActive (true);
	}
}
