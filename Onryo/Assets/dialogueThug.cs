﻿using System.Collections;
using System.Collections.Generic;
using Dialog;
using UnityEngine;
using UnityEngine.UI;

public class dialogueThug : BaseNPCBehaviour {

	public EnemyAI enemy;
	public bool met;
	public bool speak;

	public bool nextEnd = false;
	public bool displayThing = false;
	public dialogueOperator dOperator;
	public dialogueNPC npcOp;
	public GameObject newNPCState;
	public GameObject anim;

	// Simple Dialogues //
	// This is a basic example of how you can use the dialogue system. //
	void Start() {
		//npc.SetTree("Firstmeeting"); //This sets the current tree to be used. Resets to the first node when called.
		//	Display();
	}

	void Update(){

		if (enemy.hostile == true) {
			Debug.Log ("player");
			anim.SetActive (true);
			this.gameObject.SetActive (false);
		}
		/*if (displayThing) {
			Display ();
			if (nextEnd == true) {
				backPanel.SetActive (false);
				nextTreeButton.SetActive (true);
			} else {
				backPanel.SetActive (true);
				nextTreeButton.SetActive (false);
			}
		}*/
	}




	public void Display()
	{

		}

	void OnTriggerEnter(Collider col){


		if(col.tag == "characters" && col.GetComponent<factions>().faction == "Player"){

			if (met) {
				dOperator.conversation = this.gameObject;
				nextEnd = false;
				displayThing = true;
				Display ();
				met = false;
			}
		}
	}

	void OnTriggerStay(Collider col){


		if(col.tag == "characters"){


			if (Input.GetKeyDown (KeyCode.E)) {
				if (!met) {
					dOperator.conversation = this.gameObject;
					nextEnd = false;
					displayThing = true;
					Display ();
				} else {
					//dOperator.conversation = this.gameObject;
					//TalkAgain ();
				}

			}
		}
	}
}
